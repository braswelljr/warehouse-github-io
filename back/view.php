<?php

use function PHPSTORM_META\type;

include_once "databasehandler.php";

$sql = "SELECT * from customer";
$query = "SELECT * FROM products";

$result = mysqli_query($connect, $query);
$exe = mysqli_query($connect, $query);

if (isset($_Get['edit'])) {

    $email = $_POST["email"];
    $product = $_POST["product"];
    $type = $_POST["type"];
    $quantity = $_POST["quantity"];
    $description = $_POST["description"];
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="../bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <title>Document</title>
</head>

<body>
    <div class="container-fluid">
        <table class="table table-striped" style="margin-top: 30px;">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">E-mail</th>
                    <th scope="col">Product</th>
                    <th scope="col">Product Type</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Description</th>
                    <th scope="col">Query</th>
                </tr>
            </thead>
            <tbody>
                <?php if (mysqli_num_rows($result)  > 0) : ?>
                    <?php while ($row = mysqli_fetch_assoc($result)) : ?>
                        <?php if (mysqli_num_rows($exe) > 0) : ?>
                            <?php while ($row = mysqli_fetch_assoc($exe)) : ?>
                                <tr>
                                    <th scope="row"><?php echo $row["email"]; ?></th>
                                    <td><?php echo $row["product"]; ?></td>
                                    <td><?php echo $row["type"]; ?></td>
                                    <td><?php echo $row["quantity"]; ?></td>
                                    <td><?php echo $row["description"]; ?></td>
                                    <td>
                                        <nav aria-label="breadcrumb">
                                            <ol class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="../front/insert.html?company=<?php echo $row['company'] ?>">Insert</a></li>
                                                <li class="breadcrumb-item"><a href="view.php?company=<?php echo $row['company'] ?>">View</a></li>
                                                <li class="breadcrumb-item"><a href="delete.php?company=<?php echo $row['company'] ?>">Delete</a></li>
                                            </ol>
                                        </nav>
                                    </td>
                                </tr>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
    </div>
</body>

</html>