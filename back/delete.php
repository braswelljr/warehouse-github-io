<?php

include "databasehandler.php";

$email = (int) $_GET['email'];

$sql = "DELETE FROM customer  WHERE email = $email LIMIT 1";

if (mysqli_query($connect, $sql)) {
    session_start();

    $message = "<script>alert('Record deleted successfully')</script>";

    $_SESSION['message'] = $message;
    header("Location: customertable.php");
} else {
    echo "Error deleting record: " . mysqli_error($connect);
}

mysqli_close($connect);
