<?php

include_once "databasehandler.php";

$sql = "SELECT * from products LIMIT 1";

$execute = mysqli_query($connect, $sql)

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="../bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <title>Document</title>
</head>

<body>
    <div class="container-fluid">
        <table class="table table-striped" style="margin-top: 30px;">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Products Name</th>
                    <th scope="col">Product type</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Item Description</th>
                    <th scope="col">Query</th>
                </tr>
            </thead>
            <tbody>
                <?php if (mysqli_num_rows($execute)  > 0) : ?>
                    <?php while ($row = mysqli_fetch_assoc($execute)) : ?>
                        <tr>
                            <th scope="row"><?php echo $row['product']; ?></th>
                            <td><?php echo $row['type']; ?></td>
                            <td><?php echo $row['quantity']; ?></td>
                            <td><?php echo $row['description']; ?></td>
                            <td>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="../front/insert.html?company=<?php echo $row['company'] ?>">Update</a></li>
                                        <li class="breadcrumb-item"><a href="delete.php?company=<?php echo $row['company'] ?>">Delete</a></li>
                                    </ol>
                                </nav>
                            </td>
                        </tr>
                        <?php var_dump($row); ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</body>

</html>