<?php

include_once "databasehandler.php";

$sql = "SELECT * from customer";

$result = mysqli_query($connect, $sql)
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="../bootstrap-4.3.1-dist/css/bootstrap.css">
  <link rel="stylesheet" href="../bootstrap-4.3.1-dist/css/bootstrap-grid.css">
  <title>Document</title>
</head>

<body>
  <div class="container-fluid">
    <table class="table table-striped" style="margin-top: 30px;">
      <thead class="thead-dark">
        <tr>
          <th scope="col">firstname</th>
          <th scope="col">lastname</th>
          <th scope="col">company</th>
          <th scope="col">phone</th>
          <th scope="col">email</th>
          <th scope="col">comment</th>
          <th scope="col">Query</th>
        </tr>
      </thead>
      <tbody>
        <?php if (mysqli_num_rows($result)  > 0) : ?>
          <?php while ($row = mysqli_fetch_assoc($result)) : ?>
            <tr>
              <th scope="row"><?php echo $row["firstname"]; ?></th>
              <td><?php echo $row["lastname"]; ?></td>
              <td><?php echo $row["company"]; ?></td>
              <td><?php echo $row["phone"]; ?></td>
              <td><?php echo $row["email"]; ?></td>
              <td><?php echo $row["comment"]; ?></td>
              <td>
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../front/insert.html?company=<?php echo $row['company'] ?>">Insert</a></li>
                    <li class="breadcrumb-item"><a href="view.php?company=<?php echo $row['company'] ?>">View</a></li>
                    <li class="breadcrumb-item"><a href="delete.php?company=<?php echo $row['company'] ?>">Delete</a></li>
                  </ol>
                </nav>
              </td>
            </tr>
          <?php endwhile; ?>
        <?php endif; ?>
  </div>
</body>

</html>