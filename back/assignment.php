<? php
  
  include "databasehandler.php";

   if (isset($_Get['edit'])){
       $id = $_GET['edit'];
       $edit_state = true;
       
       $rec = mysqli_query($db, "SELECT * FROM info WHERE id=$id");
       $record = mysqli_fetch_array($rec);
       $name = $record['email'];
       $address = $record['a'];
       $index = $record['index'];
       $id = $record['id'];
       
   }

   
?>
    <!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="../bootstrap-4.3.1-dist/css/bootstrap.css">
	<link rel="stylesheet" href="/bootstrap-4.3.1-dist/css/bootstrap-grid.css">
	<script src="../bootstrap-4.3.1-dist/jquery-3.3.1..slim.min.js"  crossorigin="anonymous"></script>
<script src="../bootstrap-4.3.1-dist/popper-1.14.7.min.js"  crossorigin="anonymous"></script>
<script src="../bootstrap-4.3.1-dist/js/bootstrap.min.js"  crossorigin="anonymous"></script>
<title> RECORDS  </title>
</head>

<body text-align="center">
    <?php if (isset($_SESSION['msg'])): ?>
           <div class="msg">
         <?php
               echo $_SESSION['msg'];
               unset ($_SESSION['msg']);
               ?>
    
    </div>
    <?php endif ?>
 <table>
    <tr>
        <th>NAME</th>
        <th>ADDRESS</th>
        <th>INDEX NUMBER</th>
        <th colspan="2">ACTION</th>
    </tr>
  <tbody> 
      <?php while($row = mysqli_fetch_array($results)){?>
    
     <tr>
        <td><?php echo $row['name'];?></td>
        <td><?php echo $row['address']; ?></td>
        <td><?php echo $row['index']; ?></td>
        <td><a  class="edit_btn" href="assignment.php?edit=<?php echo $row['id']; ?>">EDIT</a></td>
         
        <td><a class="del_btn" href="server.php?del<?php echo $row['id']; ?>">DELETE</a></td>
     </tr> 
      
      <?php 
}
    ?>
  </tbody>
 
 </table>
   
 <form method="post" action="server.php">
     <input type="hidden" name="id" value="<?php echo $id;?>">
     <div class="input-group">
     <label>Name</label>
      <input type="text" name="name"  value="<?php echo $name; ?>">
     </div>
       <div class="input-group">
     <label>Address</label>
      <input type="text" name="address"  value="<?php echo $address; ?>">
     </div> 
       <div class="input-group">
     <label>Index</label>
      <input type="number" name="index-number"  value="<?php echo $index; ?>">
     </div> 
      <div class="input-group">
      <button type="submit" name="save" class="btn">SAVE</button>
     
     </div> 
   </form>  
</body>
</html>